const St = imports.gi.St;
const PopupMenu = imports.ui.popupMenu;
const GObject = imports.gi.GObject;
const Clutter = imports.gi.Clutter;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();
const ConfirmDialog = Me.imports.confirmDialog;
const Utils = Me.imports.utils;

const Gettext = imports.gettext.domain("gnome-trash");
const _ = Gettext.gettext;

var NoticeBar = GObject.registerClass(
  class NoticeBar extends PopupMenu.PopupBaseMenuItem {
    _init(parent) {
      super._init({
        activate: false,
        hover: false,
        style_class: 'gt-action-box',
      })

      let notice1 = new St.Label({ text: _("Moved to: https://github.com/b00f/gnome-trash") });
      let notice2 = new St.Label({ text: _("No more update here") });

      this.actor.add(notice1);
      this.actor.add(notice2);
    }
  }
);